dota2cat
===

This is an updated version of the [`dotacat`](https://crates.io/crates/dotacat) crate. The original crate's name was 
a pun on `lolcat` (because DOTA is a similar game to LOL), so I named mine "DOTA 2 cat" :L

The only real difference between this crate and the original is that I've updated the dependencies, modernised it to 
use Rust edition 2021, and fixed an issue with a breaking change in [`clap`](https://crates.io/crates/clap).

## Installation
`dota2cat` can be installed with `cargo`, Rust's package/dependency manager:

```bash
cargo install dota2cat
```

To more easily facilitate replacing `dotacat` with `dota2cat`, the executable is still named `dotacat`.

The original README continues below.

# dotacat

`dotacat` is meant to be a replacement to `lolcat`. If you're not aware, `lolcat` is a rather silly program which behaves like `cat`, but produces a colourful, rainbow output.

![](screenshot.png?raw=true)

## Why?

Speed!

```
$ time echo hi | lolcat
real    0m0.422s
user    0m0.393s
sys     0m0.028s
```

I use lolcat in my `.bashrc` file, so this amount of time is not ideal for me.

In contrast:

```
time echo hi | dotacat

real    0m0.045s
user    0m0.030s
sys     0m0.020s

```

## Why the name?

Because Dota is better than LoL (According to people - I play neither)

## Installation

If you have cargo installed, just run: `cargo install dotacat`

If not, head over to the [releases page](https://gitlab.scd31.com/stephen/dotacat/-/releases/) and download the latest release. Then run:

```
chmod +x dotacat
sudo mv dotacat /usr/local/bin
```

## Usage

```
USAGE:
    dotacat [FLAGS] [OPTIONS] [files]...

ARGS:
    <files>...    Files to concatenate(`-` for STDIN)

FLAGS:
    -h, --help       Prints help information
    -i, --invert     Invert fg and bg
    -V, --version    Prints version information

OPTIONS:
    -F, --freq <freq>        Rainbow frequency [default: 0.1]
    -S, --seed <seed>        Rainbow seed, 0 = random [default: 0.0]
    -p, --spread <spread>    Rainbow spread [default: 1.0]

Examples:
	dotacat f - g	Output f's contents, then stdin, then g's contents.
	fortune | dotacat	Display a rainbow cookie.
```
